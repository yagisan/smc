# What is SMC (Skyrim Mod Combiner)?

SMC is a free, fast and light-weight mod combiner for Skyrim, designed to be as universal and user-friendly as possible.
> SMC was initially made as extended GUI wrapper for a batch-file from [Texture Pack Combiner](http://skyrim.nexusmods.com/mods/20801) by **Cestral**,
but uses its own updated lists now for compatibily with newer and additional mods.

---

   ![SMC Screenshot](http://s4.postimg.org/lqqf6crst/SMC_Screenshot.png "SMC Screenshot")

---

## SMC features:

  + Written fully in AutoIt, so it is **Windows-only**;
  + Comes with a **[7-Zip](http://www.7-zip.org/)** implemented;
  + Uses **SQLite** as an internal database;
  + Works with _archived_ and/or _extracted_ mods;
  + Automatically installs _compatibility patches_, if needed;
  + Dynamically calculates _space required_;
  + Dynamically indicates _mod availability_;
  + Optionally packs the combined output into _NMM-compatible archive_;
  + Optionally _optimizes_ combined textures with **[DDSopt](http://skyrim.nexusmods.com/mods/5755)** and/or _creates BSA-archive_ with **[BSAopt](http://skyrim.nexusmods.com/mods/247)** by **Ethatron**;
  + Automatically creates readable _detailed log_.

---

## Downloading & updating:

  * Latest SMC version can always be found in **[Downloads](https://bitbucket.org/drigger/smc/downloads)** section,  
  * There is also a new **[SMC homepage on Nexus](http://www.nexusmods.com/skyrim/mods/51467)** available,  
  * Or you can simply click **[here](https://bitbucket.org/drigger/smc/downloads/SMC_1.2.0.2.7z)**,  
  * And don't worry, SMC will update itself once installed (but you can still check this page every now and then, just in case).  

---

## Special notice:

  * Due to specific changes in SMC 1.1.0.0 autoupdate feature in previous versions _won't work as intended_ and SMC _won't be able to update itself automatically_.  
If you have pre-1.1.0.0 version, please, _**download latest version manually**_!

---

## Latest changes:

* > [1.2.0.2 @ 09.04.2014]  
(F) Compatibility with updated AutoIt components & bugfixes  
(U) SQLite.dll updated to the latest 3.8.4.3  
* > [1.2.0.1 @ 31.03.2014]  
(F) Hotfix for a "variable used without being declared" error  
(U) SQLite.dll updated to the latest 3.8.4.2  
* > [1.2.0.0 @ 28.03.2014]  
(+) NMM-compatible archive creation option is now available for users without NMM installed  
(+) Option for users with NMM installed to create NMM-compatible archive directly in NMM Skyrim mods folder (available through dialog window)  
(+) Compatible patches tab in mod settings editor  
(+) DDSoptimize file skipping feature finalized  
(+) File exclusion feature for new lists format (5th parameter)  
(F) Bugfixes  
(E) NMMCreate/NMMUseCompression renamed to CreateArchive/NMMUseCompression  
* > [1.1.0.1 @ 20.03.2014]  
(+) DDSoptimize file skipping feature  
* > [1.1.0.0 @ 17.03.2014]  
(+) Old TPC-styled batch files changed to new .lst (packed .ini)  
(+) Change compatible list used by SMC option (hold ctrl during app start)  
(+) Files overwriting mechanism  
(F) Better DLC detection  
(F) Faster list initialization  
* > [1.0.0.2 @ 24.02.2014]  
(F) Fixed incompatibility with x86 OS introduced in previous version  
* > [1.0.0.1 @ 21.02.2014]  
(E) NMMUseCompression option can now only be selected if NMMCreate is enabled  
* > [1.0.0.0 @ 13.02.2014]  
(+) Main TPC batch-file auto-download  
(F) CPU high consumption bug  
* > [0.9.0.5 @ 03.02.2014]  
(F) A bit more bugfixes  
* > [0.9.0.4 @ 01.02.2014]  
(F) A couple of small bugfixes  
* > [0.9.0.3 @ 01.02.2014]  
(+) NMM detection logic enhanced  
* > [0.9.0.2 @ 01.02.2014]  
(+) NMM detection status in logs  
* > [0.9.0.1 @ 31.01.2014]  
(+) Loading animation  
(E) Renamed some functions & cleaned code a little bit  
* > [0.9.0.0 @ 28.01.2014]  
(E) Application name changed to SMC (Skyrim Mod Combiner)  
(E) Application rebased to https://bitbucket.org/drigger/smc/  

---

## Current mods to-do list:

* About Horses  
* aMidianBorn Book of Silence  
* aMidianBorn Hide and Studded Hide  
* aMidianBorn Hide of the Savior  
* aMidianBorn imperial light and studded  
* aMidianBorn Iron and Banded Armor  
* aMidianBorn Scaled Armor  
* aMidianBorn Steel Armor  
* aMidianBorn stormcloak officer armour  
* aMidianBorn wolf armor and skyforge weapons  
* Better Beast Races  
* Better Male Feet  
* Better males  
* Coverkhajiits  
* DCE - Realistic Male Face  
* Ethereal Elven Overhaul  
* EWIs little Texture Pack  
* Female Vampires Have Fangs  
* FemFeet Redesigned  
* Geonox Male Face -Blackbugfix  
* Glyphic Enchantment Effect  
* Hvergelmirs Armor Retexture - HAR  
* Hvergelmirs Shield Retexture - HSR
* Hvergelmirs Steel Armory  
* Improved Foot Wraps for females  
* Improved Weapon Impact EFFECTS Correct Metal  
* Proper Dark brotherhood armor retex  
* Royal Bloodskal Pattern  
* Skill Interface Retexture  
* Skyrim Improved Puddles - SIP  
* The 418th Step  
* Thieves Guild Armor HD revival  
* Weapon Retexture Project - WRP  
* XCE - Dawnguard
* XCE - Warpaint and Dirt  
* XCE - Xenius Character Enhancement  
* XRC HD Imperial Armory  

---

## P.S.

_If you like what I do you can buy me a drink by clicking this:_ [![Donate with PayPal Button](https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif "Donate with PayPal")](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=LVHX9P9F7UHHQ)